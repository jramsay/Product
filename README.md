# GitLab Product Department Repository
* [Handbook](https://about.gitlab.com/handbook/product/)

## GitLab Product Process

This repository also includes code to automate the scheduled creation of issues for the Product team.

## Usage

Update [issues.yml](https://gitlab.com/gitlab-com/Product/-/blob/main/config/issues.yml) and [section.yml](https://gitlab.com/gitlab-com/Product/-/blob/main/config/sections.yml) with the appropriate variables:
* **Issue Title** = Issue.name + (optionally Section.name)
* **Issue Description** = Will parse template in `/templates` (for `section` scoped) or `/.gitlab/issue_templates` (for `all` scoped) directory and render as markdown
* **Date created** = Will create on the daily run that corresponds with the Issue.day_of_month or Issue.day_of_week (in [three letter abbreviated form](https://ruby-doc.org/stdlib-2.6.1/libdoc/date/rdoc/DateTime.html#method-i-strftime))
* **Issue.scope** determines at what level to create issues, either `all` or `section`. See limitations below

## Testing Locally
To test the create_issues script use the `--dry-run` arguement. Before running, make sure you set the `GITLAB_PROJECT_ID` (for the product project, it's 1641463) and the `GITLAB_API_PRIVATE_TOKEN` environment variables.

```
bundle exec ruby create_issues.rb --dry-run
```

## Testing YAML Syntax

YAML can be hard. You can check the YAML file syntax locally with these commands:

First install YAMLlint
```
gem install yamllint
```

And once installed testing the two primary yaml files
```
yamllint config/issues.yml config/sections.yml
```

## Limitations

* The only scopes that are available are `section` and `all`. We should consider adding `group` that would allow for the creation of an issue per product group. 
* In order to use multiple quick actions you have to add an extra line space between them
