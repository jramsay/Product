## Issue Tasks
* [ ] Change the title of the issue to include the actual release number for this particular kick-off - @adawar
* [ ] Add a Retrospective Thread for participating team members to provide feedback in - @adawar
* [ ] Please update the keyValue on the Kickoff page to the release we'll be kicking off. You can do this by [opening an MR for this line](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/source/direction/kickoff/template.html.md.erb#L5). - @adawar

## Group Kickoff Meetings (Completed by the 16th of the month)

**Note** - Completing these tasks by the 16th enables the Product Leadership team to have ample time to review videos and planning issues before the 18th. Please make every effort to complete and check-off the below tasks prior to the end of the day on the 15th.

### Preparation
1. Add your planning issues so your manager has an opportunity to review [New ask based on 13.1 retrospective]
1. Schedule a monthly, livestreamed, recorded Kickoff meeting for your group
1. Review the [upcoming releases page](https://about.gitlab.com/upcoming-releases/) and ensure the current list of issues makes sense. If an issue shouldn't appear there - remove the `direction` label
1. Strongly consider creating a release planning issue that links all highlighted issues 
1. Ensure the items for discussion are labeled with the `direction` label and scheduled for the upcoming milestone
1. Update the issues to ensure:
   1. The description is single-source of truth (no digging through comments required)
   1. The description contains a strong Problem Statement, Use Cases and a complete Proposal
   1. The description contains proposed designs
   1. _Note: It is highly recommended to [write your release post item content](https://about.gitlab.com/handbook/marketing/blog/release-posts/#contributing-instructions) at this time, since you've already created it for the kickoff, so you're well ahead of Key dates._
 

### Meeting
1. Consider displaying your stage or group product direction pages during this meeting
1. Try to keep the recording of the kickoff portion of this short. Even if this is scheduled as an agenda item in a regular weekly sync, please record the Kickoff portion of the discussion separately. This way others can review the kickoff playlist for all teams efficiently.
1. Review the `direction` labeled items for the milestone one-by-one, highlighting the problem statement and designs.

### After the Meeting

Once your video is recorded:

1. Post the meeting recording to GitLab Unfiltered, name it `GitLab ##.## Kickoff - Stage:Group` 
1. Assign the video to relevant Youtube playlists.  If the playlist doesn't exist yet, please create one. Include the link to your Direction page, planning issue or issue board reviewed during the video in the video description.  The following list includes the minimally required playlists to add your video to:
   1. Release specific YouTube playlist `##.## Release Kickoff` - will contain all group kickoff videos for a specific release
   1. Your group's specific YouTube playlist `GitLab Group Kickoffs - STAGE:GROUP` - will contain all of your group's specific kickoff videos
1. Add the link to the Planning issue (if appropriate) in the video description
1. Post the link to the recording in your section, stage (#s_) and group (#g_) slack channels
1. Update this issues description to include links to the video and planning issue below 
1. Add links to your "planning issue" and direct video link

#### Tasks

* [ ] Manage:Access - @mushakov - [group playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0Kp4Sv3eCZbZKtmMox3NipN-) - video - planning issue
* [ ] Manage:Compliance - @mattgonzales - [group playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0Kq48JVbIHqWFJ0IxQeIAtEM) - video - planning issue
* [ ] Manage:Import - @hdelalic - [group playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0KqqfLsMNj8EhZ9FoG1-54vH) - video - planning issue
* [ ] Manage:Optimize - @ljlane - [group playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0KpoUh9zJ7qdUNwQunTgqLxk) - video - planning issue
* [ ] Plan:Project Management - @gweaver - [group playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0KoVOLOpXpnvmWwvUCieXJ-K) - video - planning issue
* [ ] Plan:Product Planning - @cdybenko - [group playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0KrIU9UtPgDDc7M3EE3GRNfV) - video - planning issue
* [ ] Plan:Certify - @mjwood - [group playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0KpRGDNj5qwy5YOj9eEQ6Ut_) - video - planning issue
* [ ] Create:Source Code - @danielgruesso - [group playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0KqLWXT14d0V1OWRMDPfmdxU) - video - planning issue
* [x] Create:Code Review - @phikai - [group playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0KrkpA5TRbTcGLSWSxSXSrKG) - video - planning issue
* [ ] Create:Editor - @ericschurter - [group playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0Kpgl_ChXIzlTZclv4379xM1) - video - planning issue
* [ ] Create:Gitaly - @mjwood - [group playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0Ko8vcQJc4-6J_g7ohpsQIkz) - video - planning issue
* [ ] Create:Ecosystem - @deuley - [group playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0Ko8N8quAnsM9trph1tP4sVf) - video - planning issue
* [ ] Verify:CI - @thaoyeager - [group playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0KrGKYECOTyRT1R0Ji-kYFmz) - video - planning issue
* [ ] Verify:Pipeline Authoring - @dhershkovitch - [group playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0KrGKYECOTyRT1R0Ji-kYFmz) - video - planning issue
* [ ] Verify:Runner - @DarrenEastman - [group playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0KrcC7xS0y8a8S9PlElg48bZ) - video - planning issue
* [ ] Verify:Testing - @jheimbuck_gl - [group playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0KradvCrf8fL2pggEn5D3w2k) - video - planning issue
* [ ] Package:Package - @trizzi - [group playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0KoVP8cJft-Hv4kQH__aFaGS) - video - planning issue
* [ ] Release:Release - @ogolowinski - [group playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0Kqj3Dk9qD5CIrpQdLbaxrXf) - video - planning issue
* [ ] Configure:Configure - @nagyv-gitlab - [group playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0KoqJ_uOHiII_E46XzdK-eB7) - video - planning issue
* [ ] Monitor:Monitor - @sarahwaldner - [group playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0KrUe3MhOsFQGSv2uoN_Ca_B) - video - planning issue
* [ ] Secure:Static Analysis - @tmccaslin - [group playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0KpG-Vf4OE_1ZcGCfl4hqoJ1) - video - planning issue
* [ ] Secure:Dynamic Analysis - @derekferguson - [group playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0KqHxaetI2k4hZrrStTcTtCQ) - video - planning issue
* [ ] Secure:Composition Analysis - @NicoleSchwartz - [group playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0KqxyGS_Pzc8jqs4BjSzHkk4) - video - planning issue
* [ ] Secure:Fuzz Testing - @stkerr - [group playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0KqswMT728hSZ5wAVjsdCWHE) - video - planning issue
* [ ] Secure:Threat Insights - @matt_wilson - [group playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0KqdzV37vP42lXMOjbh67J8o) - video - planning issue
* [ ] Secure:Vulnerability Research - @stkerr - [group playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0KpoZluYjUAJs3bb1-86cca_) - video - planning issue
* [ ] Defend:Container Security - @sam.white - [group playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0KqRVEkGsQF-EkkToGjyKLYv) - video - planning issue
* [ ] Enablement:Distribution - @dorrino - [group playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0Kr_mLueUMhLJ7-Mc7f9eCYc) - video - planning issue
* [ ] Enablement:Geo - @fzimmer - [group playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0KofRx080zwe0VxUEv5yG_nO) - video - planning issue
* [ ] Enablement:Memory - @fzimmer - [group playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0Kq1HDOIfQ8ov6lfyJkWK2Yr) - video - planning issue
* [ ] Enablement:Search - @JohnMcGuire - [group playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0KqBMod_imaJaWeDPb7YdS6l) - video - planning issue
* [ ] Enablement:Database - @iroussos - [group playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0KqP3MYrcoQHrqPUqn_jJZSN) - video - planning issue
* [ ] Fulfillment: - @justinfarris - group playlist - video -planning issue
## Company Kickoff Meeting (Completed by the 18th of the month)

### Preparation

1. The VP of Product will be the [directly responsible individual](/handbook/people-group/directly-responsible-individuals/) for presenting and leading the meeting.
1. The format of the meeting will be a relay -starting off with the introducton and then rolling through each section in the alloted time.
2. The sequence of sections and the time per section is decided on prior to the call. VP of Product seeks inputs from the team and decides on the sequence. 
1. If a Director of Product Management is out of office on the day of the kickoff
   call, they should arrange for another Director of Product Management, or
   their delegate, to cover that section and inform the VP of Product ahead of
   time.
1. Ensure that the kickoff page is pulling issues for the upcoming milestone by updating `<% keyValue = "<milestone>"` in `source/direction/kickoff/template.html.md.erb` 
1. We should always seek feedback on the usefulness of the Kickoff Meeting. Ensure the [Kickoff Meeting Survey](https://docs.google.com/forms/d/12-QPpvggEsqCvZnDCnuCjqP53joKwjRMPy4PH-Mqp-I/viewform?edit_requested=true)
   contains relevant questions prior the Company Kickoff Meeting.

### Meeting

1. Follow the same instructions for [live streaming a Group Conversation](/handbook/people-group/group-conversations/#livestreaming-the-call) in order to livestream to GitLab Unfiltered. People Ops Specialists can help set up the Zoom webinar if not already attached to the Kickoff calendar invite. 
1. Ensure the [survey link](https://docs.google.com/forms/d/12-QPpvggEsqCvZnDCnuCjqP53joKwjRMPy4PH-Mqp-I/viewform?edit_requested=true)
   is added to the description of the live stream.
1. The person presenting their screen should make sure they are sharing a smaller window (default YouTube resolution is 320p, so don't fill your screen on a 1080p monitor. 1/4 of the screen is about right to make things readable.)
1. The VP of Product starts the meeting by:
   * Giving a small introduction about the topic of this meeting
   * Introducing the panel interviewees and themselves
   * Reminding anyone who may be watching the video/stream about how we [plan ambitiously](#ambitious-planning).
1. During the discussion about a product section
   * Each panel interviewee screen shares on their own so they can drive according to their rhythm. WE can revisit this if interviewees end up taking more time than allotted.
   * Interviewee should also be sure to use display cues (highlighting, mouse pointer movement) to indicate where in the document we are, so nobody watching gets lost.
   * The interviewee will explain the problem and proposal of listed items. If there is a UX design or mockup available, it will be shown.
   * Each Director of Product should try to have one visual item that can be opened up and looked at.
   * Be sure to mention the individual PMs for the stages and groups within your section while reviewing the issues they've highlighted.
   * Be sure you're on do not disturb mode so audio alerts do not play.
1. The VP of Product often ends the meeting by asking viewers to take the kickoff survey, quickly highlighting several high impact issues and communicating our excitement for the
   upcoming release. Consider even using one of our popular phrases: “This will be the best release ever!”

### Tasks
* Section Leader Prep - check when you have:
   1. Reviewed issue titles and descriptions for your section
   1. Reviewed your section's group kickoffs videos
   1. Prepared your notes for the live Kickoff call. Consider [using this shared notes doc](https://docs.google.com/document/d/1Bk7r2zO25OsZgg9JayVlYoVxKejrFOlkWmY7XM1h5JM/edit#heading=h.q2ylfmejjoxp). 
  * [ ] Dev & Sec Section - @david
  * [ ] Ops Section - @kencjohnston
  * [ ] Enablement Section - @joshlambert
* Pre meeting
  * [ ] Ops Section - Add a highlights comment based on your review to this issue and share in Slack - @kencjohnston
  * [ ] Ops Section - Update Link in [Ops Section Exciting Things](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/sites/handbook/source/handbook/product/categories/ops/index.html.md.erb#L170) to the highlights comment - @kencjohnston
  * [ ] Review and adjust the ordering of the YouTube Playlist - @adawar
  * [ ] Ensure that the playlist link is updated to the new one on the kickoff page - @adawar
  * [ ] Checked the survey for appropriate questions - @adawar
  * [ ] Checked recent [survey results](https://docs.google.com/forms/d/12-QPpvggEsqCvZnDCnuCjqP53joKwjRMPy4PH-Mqp-I/edit#responses)([Google Sheet](https://docs.google.com/spreadsheets/d/12AtWgVdRoVWjpf6Rvg5BXDvhDdOSyCQdgr_WbmHNeuA/edit?usp=forms_web_b#gid=1102317164)) and proposed adjustments - @adawar
* Post meeting
  * [ ] Add the livestreamed recording to the `XX.XX Release Kickoff` YouTube playlist and name it `GitLab XX.XX Kickoff - Overview` - @kristie.thomas
  * [ ] Trim the early few minutes of silence in the kick-off meeting as soon as the livestream is complete - @kristie.thomas
  * [ ] Mention the availability of the YouTube playlist in #whats-happening-at-gitlab slack channel - @adawar
  * [ ] Before closing this issue ensure Retrospective Thread items have been addressed (preferably via an MR [to this template](https://gitlab.com/gitlab-com/Product/-/blob/main/.gitlab/issue_templates/Monthly-Kickoff.md) or [other automated issue config](https://gitlab.com/gitlab-com/Product/#gitlab-product-process)) - @adawar

/assign @adawar @kencjohnston @david @joshlambert @justinfarris @kristie.thomas @jreporter @kbychu
/due in 7 days
