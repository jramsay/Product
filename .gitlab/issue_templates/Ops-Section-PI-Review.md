## :book: References

* [Handbook Page](https://about.gitlab.com/handbook/product/ops-section-performance-indicators/)
* [PI YAML Data Fields](https://about.gitlab.com/handbook/product/ops-section-performance-indicators/#data)
* [PI Implementation Status Reference](https://about.gitlab.com/direction/product-analytics/#implementing-product-metrics)
* [Meeting Notes](https://docs.google.com/document/d/1ff7V_SRdJDYWzfCMAFLGYWxrMHAlTds4No6ROQZP2jo/edit#)
* [Past Prep Issues](https://gitlab.com/gitlab-com/Product/-/issues?scope=all&utf8=%E2%9C%93&state=all&label_name[]=PI%20Review%20Prep&label_name[]=section%3A%3Aops)


## :dart: Intent

* [Ops Section PIs - Intent](https://about.gitlab.com/handbook/product/ops-section-performance-indicators/#intent)

## :white_check_mark: Tasks

### :o: Opening Tasks
* [ ] Set a due date to this issue as 2 business days prior to the scheduled review to make time for Leadership async review - @kencjohnston
* [ ] Add a restrospective thread to this issue - @kencjohnston
* [ ] Update the [Meeting Notes](https://docs.google.com/document/d/1ff7V_SRdJDYWzfCMAFLGYWxrMHAlTds4No6ROQZP2jo/edit#) with the timing and this Prep Issue
* [ ] Review the [update actions](#star-performance-indicator-yaml-updates) and ensure they are accurate per the [Data definitions](https://about.gitlab.com/handbook/product/ops-section-performance-indicators/#data) - @kencjohnston
 
### :heavy_check_mark: Other Tasks
* [ ] Check remaining follow ups from last review - @kencjohnston

### :star: Performance Indicator YAML Updates

Each product stage or group should update the [Ops Section Performance Indicators YAML file](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/data/performance_indicators/ops_section.yml) inline with the [Data properties](https://about.gitlab.com/handbook/product/ops-section-performance-indicators/#data) including:

* Review your `definition:` to include both what specifically is being measured and the **rationale** for why you chose that measurement
* Update your `health:` level and `implementation:` status
* Update any `funnel:` to reference stage/group direction or handbook page for the growth model and funnel
* Update any `urls:` to reference additional Sisense dashboards for more details
* Update `lessons:` `learned:` with `Insight -` prefixed comments about insights gained from the last month of data
* Update `lessons:` `learned:` with `Improvement -` prefixed comments about planned improvements to reach your goals over the next month
* If there have been recent updates to the definition, rationale, growth model, funnel highlight those and include those as `reasons:` under `health:`
* If you don't have a defined metric, rationale, growth model or funnel in the handbook reference issues for creating those and provide status on blockers in the `reasons:` under `implementation:`.

_Note_ - These instructions are based off of currenting understanding of the values defined in the [Data section](https://about.gitlab.com/handbook/product/ops-section-performance-indicators/#data) for Performance Indicator pages. They may change.

#### Group Updates

* [ ] Update Section Wide - @kencjohnston - MR
* [ ] Update Verify:CI - @thaoyeager - MR
* [ ] Update Verify:Pipeline Authoring - @dhershkovitch - MR
* [ ] Update Verify:Runner - @DarrenEastman - MR 
* [ ] Update Verify:Testing - @jheimbuck_gl - MR
* [ ] Update Package - @trizzi - MR
* [ ] Update Release - @ogolowinski - MR 
* [ ] Update Configure - @nagyv-gitlab - MR
* [ ] Update Monitor - @sarahwaldner - MR

### :package: Pre-Meeting Tasks
* [ ] Consider pre-recording an overview - @kencjohnston
* [ ] Ask questions in the [doc](https://docs.google.com/document/d/1ff7V_SRdJDYWzfCMAFLGYWxrMHAlTds4No6ROQZP2jo/edit#) - @kencjohnston
* [ ] Ping KT to confirm content is ready for review in the [doc](https://docs.google.com/document/d/1ff7V_SRdJDYWzfCMAFLGYWxrMHAlTds4No6ROQZP2jo/edit#) 
* [ ] Share the upated PI handbook page in the #ops-section and #product Slack channels 2 business days in advance of the review and encourage async participation - @kencjohnston

### :night_with_stars: Post-Meeting Tasks
* [ ] Capture actions discussed in the review in the [follow-up actions](#follow-up-actions) section

### :x: Closing Tasks
* [ ] Create issues for any uncompleted follow-up-actions
* [ ] Make [adjustments to the template](https://gitlab.com/gitlab-com/Product/-/edit/main/.gitlab/issue_templates/Ops-Section-PI-Review.md) based on the retrospective thread - @kencjohnston
* [ ] Add a highlights comment and share in Slack
* [ ] Update the link in [Exciting Things](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/sites/handbook/source/handbook/product/categories/ops/index.html.md.erb#L170) to the new highlights comment

### Follow Up Actions
* [ ] ACTION @

/assign @kencjohnston @kbychu @thaoyeager @dhershkovitch @DarrenEastman @jheimbuck_gl @trizzi @jreporter @ogolowinski @nagyv-gitlab @sarahwaldner

/label ~section::ops ~"PI Review Prep"
