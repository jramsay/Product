## :book: References

* [Handbook Page](https://about.gitlab.com/handbook/product/dev-section-performance-indicators/)
* [Meeting Notes](https://docs.google.com/document/d/1h8j9Qzhfyv4KRryYAM362jqdn5A0OiXX-DHS1aaY2Xs/edit)
* [PI .yml file](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/data/performance_indicators/dev_section.yml)

## :dart: Intent
We are organizing for and regularly reviewing Performance Indicators (PIs) in order to enable a dialog between ourselves (all of R&D) about most effective use of our R&D efforts towards the most impactful improvements.

As a communication tool, Performance Indicators are only as useful as the range of the audience. As a result we should:

* store PI metrics, funnels, growth models, status and next steps handbook first (avoid content only living in slides)
* expose our product groups to PI metrics regularly
* communicate transparently about adjustments to PIs within our groups
* reference PIs in planning issues and prioritization discussions

## :white_check_mark: Tasks

### :o: Opening Tasks

* [ ] Set a due date to this issue as 1 business day prior to the scheduled review - @david
* [ ] Add a restrospective thread to this issue - @david

### :star: Performance Indicator YAML Updates

Each product stage or group should update the [Performance Indicators YAML file](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/data/performance_indicators/dev_section.yml) with:

* Update your `health:` and `instrumentation:` levels
* Update any `urls:` to reference stage/group direction or handbook pages for the rationale, growth model and funnel
* Update any `urls:` to reference additional Sisense dashboards for more details
* Update `health:` `reasons:` with `Insight -` prefixed comments about insights gained from the last month of data
* Update `health:` `reasons:` with `Improvement -` prefixed comments about planned improvements to reach your goals over the next month
* If there have been recent updates to the definition, rationale, growth model, funnel highlight those and include those as `reasons:` under `health:`
* If you don't have a defined metric, rationale, growth model or funnel in the handbook reference issues for creating those and provide status on blockers in the `reasons:` under `instrumentation:`.

### :star: Expectations
1. Ensure your graph is refreshed
1. Ensure your graph looks correct and if it's not, understand why and change it
1. Have a comprehensive understanding of your graphs (understand history, challenges with the data, etc) and document directly in the .yml file
1. Ensure there are no graph labeling or title errors
1. Ensure you have a target set and you know how that target was set. If you can't have a target, in the target field, please explain why you don't have a target and when you would expect to have one.

#### Group Updates

**Section**
* [ ] Section TMAU - @david [MR Link]()

**Manage**
* [ ] Overall Manage Review 
* [ ] Access @mushakov  - [MR Link]()
* [ ] Import @hdelalic - [MR Link]()
* [ ] Compliance @stkerr - [MR Link]()
* [ ] Optimize @ljlane - [MR Link]()

**Plan**
* [ ] Overall Plan Review
* [ ] Project Management @gweaver  - [MR Link]()
* [ ] Portfolio Management @cdybenko - [MR Link]()
* [ ] Certify @mjwood - [MR Link]()

**Create**
* [ ] Overall Create Review 
* [ ] Source Code @danielgruesso - [MR Link]()
* [ ] Gitaly @mjwood - [MR Link]()
* [ ] Code Review @phikai - [MR Link]()
* [ ] Editor @ericschurter - [MR Link]()
* [ ] Ecosystem @deuley - [MR Link]()

### :x: Closing Tasks
* [ ] Make [adjustments to the template](https://gitlab.com/gitlab-com/Product/-/blob/main/.gitlab/issue_templates/Dev-Section-PI-Review.md) based on the retrospective thread - @david
* [ ] Activate on any retrospective thread items - @david
