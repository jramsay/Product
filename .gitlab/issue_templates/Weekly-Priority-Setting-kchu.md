## :dart: Intent
Ensure I'm focusing my time and team on the most important things week to week.

## Tasks

### :wave: Open
* [ ] Create retrospective Thread

### :exclamation: Extras
* [ ] If first week of the month - Create issues for any un-accounted for [Quarterly Priority](https://gitlab.com/kbychu/README/-/blob/master/priorities.md#quarterly-priorities) actions
* [ ] If first week of the month - Review all Open issues and MRs for delegation
* [ ] If first week of the quarter - Review and adjust [Quarterly Priority](https://gitlab.com/kbychu/README/-/blob/master/priorities.md#quarterly-priorities) - set and track quarterly goals
* [ ] If after the 8th and before the 15th, review Planning issues
  * [ ] Review Configure Planning Issue
  * [ ] Review Monitor Planning Issue
  * [ ] Review Release Planning Issue
* [ ] If after the 14th and before the 18th, review Release Post Items

### :pencil2: Review
* [ ] Review [Quarterly Priorities](https://gitlab.com/kbychyu/README/-/blob/master/Priorities.md#quarterly-priorities), [Calendar](https://calendar.google.com/calendar/u/0/r/week), and [Issue List](https://gitlab.com/dashboard/issues?assignee_username=kbychu)
* [ ] Edit [Weekly Priorities](https://gitlab.com/kbychu/README/-/blob/master/priorities.md)

### :mega: Communicate
* [ ] Communicate them in Slack (on threads in #product and #ops-section or in #s_release, #s_configure, #s_monitor)

### :x: Close
* [ ] Updates to [this template](https://gitlab.com/gitlab-com/Product/-/edit/main/.gitlab/issue_templates/Weekly-Priority-Setting-kchu.md) from the Retro Thread

/assign @kbychu

/label ~"section::ops" 

