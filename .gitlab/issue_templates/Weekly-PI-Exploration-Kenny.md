## Intent
Ensure I'm exploring our Section Wide PIs every week to gain a better understanding of them and our Product performance.

## Tasks

### :wave: Open
* [ ] Create retrospective Thread

### :exclamation: Extras
* [ ] If first week of the month - Consider broader insights from the month
* [ ] If first week of the quarter - Consider broader insights from the quarter

### :pencil2: Review
* [ ] Review [Ops Section PI Dashboard](https://app.periscopedata.com/app/gitlab/781120/Ops-Section-Dashboard)

### :map: Explore
* [ ] Explore stages, groups, feature dashboards in depth. Build own additional charts in that exploration.
* [ ] Particularly focus on the funnel Runner->Pipeline Authoring->CI->Testing->Release funnel

### :thinking: Reflect
* [ ] Ask PMs in Slack (not this issue) about insights, questions 

### :x: Close
* [ ] Updates to [this template](https://gitlab.com/gitlab-com/Product/-/edit/main/.gitlab/issue_templates/Weekly-PI-Exploration-Kenny.md) from the Retro Thread

/assign @kencjohnston

/label ~"section::ops" 

/epic https://gitlab.com/groups/gitlab-com/-/epics/1338
