**Next Product Key Review:**


**Link to Google slides for Key Meeting - Product:**

### Tasks: 

* Review the KPI slides for metrics you own and add any necessary commentary either to the graph slides or the key takeaways slide 
  * [ ]  @mkarampalas 
  * [ ]  @jstava 
  * [ ]  @s_awezec
  * [ ]  @kokeefe
* Update your Section Highlights Slide
  * [ ]  @david
  * [ ]  @kencjohnston
  * [ ]  @joshlambert
  * [ ]  @justinfarris
  * [ ]  @hilaqu
* [ ] Update the FY21 Q3 okr slides that you own - @kokeefe
* [ ] Update the FY21 Q3 okr slides that you own - @hilaqu
* [ ] Update the NPS related slides -  @fseifoddini 
* [ ] Update the Category Maturity KPI related slides -  @fseifoddini
* [ ] Updates the slides you own - @adawar
* Review relevant Data Team Key Meeting Slides
  * [ ]  @kokeefe
  * [ ]  @mpeychet_
  * [ ]  @dpeterson1 
* [ ] Review full deck before Key Meeting - @sfwgitlab 
* [ ] Add a "Retrospective Thread" comment to this issue - @kristie.thomas
* [ ] After each Key Meeting [update this Key Meeting prep issue template](https://gitlab.com/gitlab-com/Product/edit/master/.gitlab/issue_templates/Product-Key-Meeting-Prep.md) with any added items - All
    - For next month, make a copy of this month's Key Meeting Slides and save them in the [Key Meeting - Product Folder](https://drive.google.com/drive/folders/1fnOD9MXwfgzscquTPUHQLSTm1xi5-4Bg)
* [ ] Update the Health Score of the OKR issues if they have changed - @kristie.thomas
* [ ] Update the [issue template](https://gitlab.com/gitlab-com/Product/-/edit/main/.gitlab/issue_templates/Product-Key-Meeting-Prep.md) with any improvements suggested in the Retrospective Thread - @kristie.thomas

Thank you all for your help!


/assign @kristie.thomas
