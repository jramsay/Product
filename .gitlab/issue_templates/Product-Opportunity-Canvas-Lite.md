<!--As apart of [Problem Validation](https://about.gitlab.com/handbook/product-development-flow/#validation-track) in the Product Development Flow, Product Managers are responsible for assessing the opportunity, impact and risk of proceeding with the investment of introducing a new feature, which is typically presented in an [opportunity canvas](https://about.gitlab.com/handbook/product-development-flow/#opportunity-canvas). Often, Product Managers are extending existing functionality and are looking to understand quickly how to made tradeoffs between the next iterations of features. This template adapts the traditional Opportunity Canvas to be more for feature::additions of categories instead of improving the Category Maturity of a Group.-->

## Instructions

<details><summary>Click to expand</summary>

- [ ] Create a :recycle: Retrospective Thread to capture future improvements to this issue template
- [ ] Complete the sections in the issue template below 
- [ ] Link any relevant issues or research 
- [ ] Assign to your Manager
- [ ] Assign to VP and EVP of Product for approval `@sfwgitlab` `@adawar`
- [ ] Include VP of UX for visibility `@clenneville`
- [ ] Record a walk-through of this issue (optional)
- [ ] Make a copy of the [Opportunity Lite Canvas Meeting - TEMPLATE](https://docs.google.com/document/d/1sHY-sToF9TXH-RWZryF967F_w8jI05PDGf-YcUOHrTc/edit#) and fill out the required points.
- [ ] Schedule the review. <!-- Reach out to KT in slack #product to schedule a review. Include a link to the copy of the agenda you made above. -->
- [ ] Once issue is approved, add the label `~oppty-lite::approved` close issue and share in relevant slack channels
- [ ] Follow up on the :recycle: Retrospective Thread items and [propose changes to this issue's template](https://gitlab.com/gitlab-com/Product/-/edit/master/.gitlab/issue_templates/Product-Opportunity-Canvas-Lite.md)

Note: If issue is not approved, the issue can be closed without adding any labels.  

</details>

## Recorded Walk-Through
<!-- OPTIONAL - record yourself walking through the issue so that those who read this in advance may choose to watch, listen or read it. -->

## Why Canvas Lite?
<!-- Describe why you chose this lite format https://about.gitlab.com/handbook/product-development-flow/#opportunity-canvas-lite For example: I am using the [Opportunity Canvas Lite](https://about.gitlab.com/handbook/product-development-flow/#opportunity-canvas-lite) format because this is for an existing category and feature for a understood problem and the solution proposed is leveraging existing patterns. -->

## Customer Pain and Problem 
<!-- Describe the key [persona(s)](https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas/#user-personas) or customer type and the pain that they experience most acutely. Typically, this is the JTBD of your feature -->

### Problem Details

<!-- Add more details about the problem stated above -->

## Proposed Solution
<!-- Explain the proposed solution. It is suggested to include a low-fidelity mockup if possible. You can do this under a collapsing section to save space. 

<details><summary>Mockup</summary>
</details>

-->

##  Business case
<!-- Describe what is the target market, size, landscape, and differentiation for this feature. These can be 1-2 sentences in length, with a high-level understanding. -->

### [RICE](https://about.gitlab.com/handbook/product/product-processes/#using-the-rice-framework)

#### [RICE](https://about.gitlab.com/handbook/product/product-processes/#using-the-rice-framework) score: 

<!-- Put your score here ##.## = (Reach x Impact x Confidence) / Effort -->

#### Reach

> What count of users are you expecting to reach with this feature?

<!-- From https://about.gitlab.com/handbook/product/product-processes/#using-the-rice-framework
10.0 = Impacts the vast majority (~80% or greater) of our users, prospects, or customers
6.0 = Impacts a large percentage (~50% to ~80%) of the above
3.0 = Significant reach (~25% to ~50%)
1.5 = Small reach (~5% to ~25%)
0.5 = Minimal reach (Less than ~5%)
 -->

<details><summary>Rationale</summary>
</details>

#### Impact

> Describe the impact this feature will have for those users.

<!-- From https://about.gitlab.com/handbook/product/product-processes/#using-the-rice-framework) 
Massive = 3x
High = 2x
Medium = 1x
Low = 0.5x
Minimal = 0.25x
-->

<details><summary>Rationale</summary>
</details>

#### Confidence

> How confident are you in the problem and solution?

<!-- From https://about.gitlab.com/handbook/product/product-processes/#using-the-rice-framework) 
High = 100%
Medium = 80%
Low = 50%
-->

<details><summary>Rationale</summary>
</details>

#### Effort

> How many person months do we estimate this will take to build?

<!-- From https://about.gitlab.com/handbook/product/product-processes/#using-the-rice-framework) -->

<details><summary>Rationale</summary>
</details>

### Business Case Questions

* Roughly what percent of those users would you expect to be Starter? Premium? Ultimate? 
  *  <!-- Answer -->
* What other companies offer a feature like this?
  *  <!-- Answer -->
* When do you intend do deliver this feature?
  *  <!-- Answer -->
* What would happen if we waited to deliver this feature later than you have planned? 
  *  <!-- Answer -->

## Qualitative Evidence 
<!-- Consider linking customer calls, other anecdotes, analyst opinions. -->

## Feature Maturity Plan 

<!--  Describe how this addition impact the maturity of the maturity for users and customers. 

Feature maturity is a different framework from [Category Maturity](https://about.gitlab.com/direction/maturity/) but uses similar language to describe progression. -->

<!--- |  Feature Maturity | Impact | Feature Lifecycle |
| --- | --- | --- |
|  Planned | No adoption | Year 0 - Not yet available |
|  Minimal | up to 10% | Year 1 - Not used at GitLab Inc |
|  Viable | up to 50% | Year 2 - Dogfooding |
|  Complete | up to 75% | Year 3 - External Users |
|  Lovable | up to 100% | Year 4 - Usable for most users |
|   |  | Year 5 - Switching reason for Category |
|   |  | Year 6 - Entry Point for Category |
|   |  | Year 7 - Differentiator in Category | -->

By offering the feature additions outlined in this Canvas Lite, we would expect to make the feature maturity: 

* [ ] Viable
* [ ] Complete
* [ ] Lovable

## Feature Development Plan 
<!-- Describe the solution implementation plan and links to any relevant issues. -->

* What is the minimal change to make progress?
* What are the next steps needed to address the problem?

## Related Epics or Issues

<!--- Reminder for labels: 

/label `~devops::` `~stage::` `~group::` `~category:` 

-->
/label ~oppty-lite:pending

/assign me
