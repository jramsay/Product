## Description
<!-- Describe what part of the product you are going to walk through. It is helpful to start with a topic or a docs link that you expect to follow along with.-->

## Brief Feature Overview and Expectations
<!-- Provide an overview of the feature as it is currently implemented in your own words, why a user would utilize this feature and your expectations of the walk-through. -->

## Results
<!-- Update the links below as they are available -->
* [GitLab Unfiltered Video]()
* [Notes Google Doc]()
* Follow up issues added as related issues

## Tasks
### Pre-Walk Through
* [ ] Create a Retrospective Thread
* [ ] Create a [new Google Doc](http://doc.new)
* [ ] Update the "Brief Feature Overview and Expectations" section
* [ ] Add appropriate labels (for example ~section and ~devops)

### During Walk Through
* [ ] Record Video
* [ ] Start with verbalizing the Brief Feature Overview and Expectations section. This should include a disclaimer section to indicate that the walkthrough may be critical of certain product areas and that the intention of the walkthrough is to highlight rough edges of the product.
* [ ] Capture notes for issues to file in the GoogleDoc

### After Walk Through
* [ ] Post recording to GitLab Unfiltered YouTube channels (include in the `XX Section`, `Product Walk Throughs - XX Section`, `Product Walk Throughs - All` playlists)
* [ ] Share recording and this issue in appropriate #s_ and #g_ channels or #product
* [ ] Create follow up issues and ping relevant product managers
* [ ] Attach issues as related to this one
* [ ] Connect the walkthrough to the relevant UX scorecards by cross-linking to them
* [ ] Contribute [improvements to this issue template](https://gitlab.com/gitlab-com/Product/-/edit/main/.gitlab/issue_templates/Product-Walk-Through.md) from the retrospective thread
* [ ] Close this issue

/label ~"product walkthrough"
