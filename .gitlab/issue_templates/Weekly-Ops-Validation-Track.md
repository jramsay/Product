## :dart: Intent
The intent of this issue is to get a view of current Validation Track activities in order to:
- Share insights learned across a Product groups
- Highlight instances where groups might be working on similar validation track activities
- Provide coaching to PMs on validation track activities

## :book: References
- [Current Validation Track Initiatives](https://about.gitlab.com/direction/ops/#current-validation-track-initiatives) 
- [Previous Issues](https://gitlab.com/gitlab-com/Product/-/issues?scope=all&utf8=%E2%9C%93&state=closed&label_name[]=Validation%20Track%20Review) 

## Tasks

### :wave: Open
* [ ] Create retrospective Thread

### :exclamation: Extras
* [ ] If first week of the month - Review previous issues for trends
* [ ] If first week of the quarter - Review previous quarter issues for trends, consider other sources

### :pencil2: Review Actions
- Consider selecting a rotating Product Group(s)
- Review the referenced Validation Track activities for that group(s)
- Look for insights that would be useful to a broader audience, including where two groups are working on a similar topic
- Highlight relevant discovers across the #ops-section and #product channels

### :heavy_check_mark: Review Complete
- [ ] @kencjohnston 
- [ ] @jreporter 
- [ ] @kbychu

### :x: Close
* [ ] Updates to [this template](https://gitlab.com/gitlab-com/Product/-/edit/main/.gitlab/issue_templates/Weekly-Ops-Validation-Track.md) from the Retro Thread

/assign @kencjohnston @jreporter @kbychu

/label ~"section::ops" ~"Validation Track Review"

/epic https://gitlab.com/groups/gitlab-com/-/epics/1338

/due Thursday
