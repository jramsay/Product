## Intent
Ensure I perform :one: [Product Walk Through](https://about.gitlab.com/handbook/product/product-processes/#walk-through) Per Week

## Tasks

### :wave: Open
* [ ] Create retrospective Thread

### Prep
* [ ] Review [Backlog of Walk Throughs](https://gitlab.com/gitlab-com/Product/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=product%20walkthrough)
* [ ] Select and prepare for a walk through, schedule the issue

### Perform
* [ ] Perform your selected walk through

### Reflect
* [ ] Consider the state of your Project and next steps walk throughs that will help you utilize future capabilities (For example - Create a new template project, start testing it, setup test results in pipeline, setup test coverage might be three or four walk throughs)
* [ ] Consider other parts of the product and workflows you haven't explored - [add issues for walk throughs](https://gitlab.com/gitlab-com/Product/issues/new?issuable_template=Product-Walk-Through) to the backlog

### :x: Close
* [ ] Updates to [this template](https://gitlab.com/gitlab-com/Product/-/edit/main/.gitlab/issue_templates/Weekly-Product-Walk-Through-Kenny.md) from the Retro Thread

/assign @kencjohnston

/label ~"section::ops" 

/epic https://gitlab.com/groups/gitlab-com/-/epics/1338
