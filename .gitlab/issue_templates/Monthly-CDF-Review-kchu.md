## :dart: Intent
Ensure I am staying on top of my team's career development

## Tasks

### :wave: Open
* [ ] Create retrospective Thread

### References
* [The CDF](https://about.gitlab.com/handbook/product/product-manager-role/#product-management-career-development-framework)
* [Topics for discussion](https://about.gitlab.com/handbook/product/product-manager-role/#topics-for-discussion-in-cdf-reviews-by-track)
* [The specific competencies](https://about.gitlab.com/handbook/product/product-manager-role/#competencies)

### :pencil2: Review
* [ ] [Sarah](https://docs.google.com/spreadsheets/d/12LgQipA6ZzyLBQu4cO5xHAF5-CHGPPlAI-pqx0RB3p4/edit#gid=1091464991) 
* [ ] [Viktor](https://docs.google.com/spreadsheets/d/1u5HzR1vFem7oYyrd5CliMDhbz-T69JiG0hoqycLp5uQ/edit#gid=1091464991)
* [ ] [Orit](https://docs.google.com/spreadsheets/d/1Bir54xHIcl5p7YIdIL3zw7AZ-L0nFYnhAs1MbsysNmg/edit#gid=1091464991)

### :x: Close
* [ ] Updates to [this template](https://gitlab.com/gitlab-com/Product/-/edit/main/.gitlab/issue_templates/Monthly-CDF-Review-kchu.md) from the Retro Thread

/assign @kbychu

/label ~"section::ops" 

