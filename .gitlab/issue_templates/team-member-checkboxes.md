#### EVPE and Directs
- [ ] @sfwilliamson - Chief Product Officer
- [ ] @adawar - VP of Product Management
- [ ] @xli1 - Principal Pricing Manager
- [ ] @fseifoddini - Principal PM, Product Operations
- [ ] @kristie.thomas - Executive Business Administrator

#### Enablement 
- [ ] @joshlambert - Group Manager
- [ ] @fzimmer  - Sr. PM, Geo
- [ ] @ljlane - Sr. PM, Distribution
- [ ] @JohnMcGuire - Sr. PM, Global Search
- [ ] @awthomas - Principal Product Manager, Infrastructure

#### Dev
- [ ] @danielgruesso - PM, Create:Source Code
- [ ] @phikai - Sr. PM, Create:Code Review
- [ ] @hdelalic - Sr. PM, Manage:Import
- [ ] @mushakov - Sr. PM, Manage:Access
- [ ] @ljlane - Sr. PM, Manage:Optimize
- [ ] @ericschurter - Sr. PM, Create:Editor
- [ ] @gweaver - Sr. PM, Plan:Project Management
- [ ] @mjwood - Sr. PM, Create:Gitaly & Plan:Certify
- [ ] @cdybenko - Sr. PM, Plan:Portfolio Management
- [ ] @deuley - Sr. PM, Ecosystem

#### Secure & Defend
- [ ] @david - Sr. Director of Product Management
- [ ] @stkerr - Pricipal PM, Secure:Fuzz Testing & Interim PM, Manage:Compliance
- [ ] @NicoleSchwartz - PM, Secure:Composition Analysis
- [ ] @tmccaslin - Pricipal PM, Secure:Static Analysis
- [ ] @derekferguson - Sr. PM, Secure:Dynamic Analysis
- [ ] @sam.white - Sr. PM, Protect:Container Security
- [ ] @matt_wilson - Sr. PM, Secure:Threat Insights

#### Ops
- [ ] @kencjohnston - Sr. Director of Product Management
- [ ] @jreporter - Group Manager, Verify
- [ ] @thaoyeager - Sr. PM, Verify:CI
- [ ] @dhershkovitch - Sr. PM, Verify:Pipeline Authoring
- [ ] @jheimbuck_gl - Sr. PM, Verify:Testing
- [ ] @DarrenEastman - Sr. PM, Verify:Runner
- [ ] @trizzi - Sr. PM, Package
- [ ] @kbychu - Group Manager, Release, Configure and Monitor
- [ ] @ogolowinski - Sr. PM, Release
- [ ] @nagyv-gitlab - Sr. PM, Configure
- [ ] @sarahwaldner - Sr. PM, Monitor

#### Growth
- [ ] @hilaqu - Director of Product, Growth
- [ ] @mkarampalas - Principal PM, Growth:Retention
- [ ] @amandarueda - PM, Growth:Fulfillment
- [ ] @timhey - Principal PM, Growth:Expansion
- [ ] @jstava - Sr. PM, Growth:Acquisition
- [ ] @s_awezec - Sr. PM, Growth:Conversion
