## Common tasks for ramping up as a PM

For some of the links below, you'll need to know your group label. Typically, this will just be in the format `group::apm`. If you are unsure of your group, please check with your manager. The links as written will take you to the APM group board as an example, simply modify the label query with your own group name to see your own board. You may also subscribe to any label, which will notify you via email each time an issue with that label is updated. 

It will also be very helpful to know the contacts for your section, stage, and group. This information is on the [categories handbook page](https://about.gitlab.com/handbook/product/categories/#devops-stages), and it's where you can find out who your assigned UX designer, engineering manager, and other roles mentioned below are.

### Week 2 (After Company Onboarding)
<!-- TODO: Change the issue id to the new PM's issue id in the first link below -->
* [ ] Complete [Product Section of your onboarding ticket](https://gitlab.com/gitlab-com/people-ops/employment/issues/UPDATE THIS VALUE #for-product-management-only)
* [ ] Familiarize yourself with the [product development timeline](https://about.gitlab.com/handbook/engineering/workflow/#product-development-timeline). It's important to know where we are in the cadence and start to work ahead as quickly as possible.
  * [ ] Create a new issue in the [Product project](https://gitlab.com/gitlab-com/Product/issues/) titled "Learning the PM Process at GitLab". Treat this issue as a discovery issue and use it as a notepad about what you're learning and where you're facing challenges.
  * [ ] Coordinate with another PM and shadow their team and customer meetings for the week.
  * [ ] Schedule a call with a different PM in your Stage to talk about their biggest three (3) "lessons learned" from doing PM @ GitLab
  * [ ] Ask at least five (5) questions in the #product Slack channel to get clarification on things you are uncertain about
  * [ ] Share one (1) of your favorite productivity "hacks" or workflows in the #product Slack channel
  * [ ] Schedule at least 2 calls, one with a frontend engineer and one with a backend engineer, to learn about their preferred workflow.
  * [ ] Edit at least 2 existing issues related to your responsibilities and add additional context or clarity to the description. Review these with your manager.
* [ ] Review your [Planning Board](https://gitlab.com/groups/gitlab-org/-/boards/1065731?label_name[]=group%3A%3Aapm) (remember, you'll need to update the group name in the query). Ensure you understand each issue in the currently underway release, as well as the next - you can find the milestone names for upcoming releases on the [schedule](https://about.gitlab.com/upcoming-releases/). If the problem we are solving isn't clear, ping your manager; if the proposal for how we would achieve it isn't clear ping your engineering manager. If the design is unclear, work with your product designer. Update issue descriptions for clarity. We have recorded an [issue grooming workshop](https://www.youtube.com/watch?v=es-SuhU_6Rc) that may help you here.
* [ ] Review the [Section Group Conversation Prep Issue Template](https://gitlab.com/gitlab-com/Product/blob/master/.gitlab/issue_templates/Group-Conversation-Prep.md) and submit an MR if it needs to be updated to include you in your new role. Assign the MR to your manager for review and merge.
* [ ] Attend your group's weekly meetings and introduce yourself to the team.
* [ ] Make any necessary adjustments to the planning board for the next release in coordination with with your engineering manager, send to your manager for async review.

### Week 3
* [ ] Review [recent release posts](https://about.gitlab.com/blog/categories/releases/), in particular the X.0 releases, for what's been changing in your stage.
* [ ] Review instructions for [engaging directly with our community of users](https://about.gitlab.com/handbook/product/#community-participation). As a Product Manager you will be [considered an expert in your group's categories](https://about.gitlab.com/handbook/marketing/community-relations/community-advocacy/#involving-experts). Participate in at least one community-advocates-driven request for expertise this week in the #product or your #s_ or #g_ channels.
* [ ] Review recent analyst research in #product-marketing and #analyst-relations slack channels - setup a meeting with Analyst Relations Manager Joyce Tompsett (`@Tompsett`) to introduce yourself, receive relevant research, and begin getting involved in analyst activities in your area. 
* [ ] Coordinate with your engineering manager on what will be delivered in the current release; ensure [release post content is merged](https://about.gitlab.com/handbook/marketing/blog/release-posts/#feature-blocks) in time.
* [ ] Using your "Learning the PM Process at GitLab" issue and one of the challenges you encountered, create an MR to add that solution to the handbook.
* [ ] Review [the direction page](https://about.gitlab.com/direction), in particular the sub-page for your stage and the direction pages for the categories you manage. You can find links directly to your category vision pages on the categories handbook page. You will be responsible for updating and maintaining your direction page with what your group is working on and why. 
* [ ] Review the [product development flow](https://about.gitlab.com/handbook/product-development-flow/index.html) and prepare any questions you have on it for discussion with your manager in your next 1x1.
* [ ] Carefully review the various labels used in the aforementioned product development flow. Ensure you understand how `workflow::xxxxx`, `direction`, `deliverable`, and `planning priority` labels work and what they are used for.
* [ ] Ping @mallen001 to open an issue for interview training
* [ ] Read about the [UX Research shadowing process](https://about.gitlab.com/handbook/engineering/ux/ux-research-training/research-shadowing). You can kick off the process when you meet with the UX Researcher for your group. Product Managers should go through research shadowing before executing UX research on their own.

### Week 4
* [ ] Organize your group issues into [Epics](https://about.gitlab.com/handbook/product/#meta-epics-for-longer-term-items) including [Category Maturity Plan Epics](https://about.gitlab.com/handbook/product/#maturity-plans). Don't hesitate to [close issues as needed](https://about.gitlab.com/handbook/product/#when-to-close-an-issue). 
* [ ] Set up regular check-ins in with your Internal Customers (also found on the categories handbook page); try to aim for a monthly cadence. 
* [ ] Reach out to users via popular issues and schedule user interviews to better understand the problems our users face.
* [ ] Complete interview training
/label ~onboarding ~Doing
