## :dart: Intent
Ensure I'm focusing my time and team on the most important things week to week.

## :book: Refrences
- [Priorities.md](https://gitlab.com/kencjohnston/README/-/blob/master/Priorities.md#)
- [Previous Issues](https://gitlab.com/gitlab-com/Product/-/issues?scope=all&utf8=%E2%9C%93&state=closed&assignee_username%5B%5D=kencjohnston&label_name%5B%5D=Weekly%20Priorities)
## Tasks

### :wave: Open
* [ ] Create retrospective Thread

### :exclamation: Extras
* [ ] If first week of the month - Create issues for any un-accounted for [Quarterly Priority](https://gitlab.com/kencjohnston/README/-/blob/master/Priorities.md#quarterly-priorities) actions
* [ ] If first week of the month - Review all Open issues and MRs for delegation
* [ ] If first week of the quarter - Review and adjust [Quarterly Priority](https://gitlab.com/kencjohnston/README/-/blob/master/Priorities.md#quarterly-priorities) - set and track quarterly goals
* [ ] If first week of the quarter - Update [Epics and Swimlanes](https://docs.gitlab.com/ee/user/project/issue_board.html#group-issues-in-swimlanes)

### :pencil2: Review
* [ ] Review [Quarterly Priorities](https://gitlab.com/kencjohnston/README/-/blob/master/Priorities.md#quarterly-priorities), [Calendar](https://calendar.google.com/calendar/u/0/r/week), [Issue Board](https://gitlab.com/groups/gitlab-com/-/boards/1353560?assignee_username=kencjohnston) and [Issue List](https://gitlab.com/dashboard/issues?assignee_username=kencjohnston)
* [ ] Edit [Weekly Priorities](https://gitlab.com/kencjohnston/README/-/edit/master/Priorities.md)

### :mega: Communicate
* [ ] Communicate them in Slack (on threads in #product and #ops-section)

### :x: Close
* [ ] Updates to [this template](https://gitlab.com/gitlab-com/Product/-/edit/main/.gitlab/issue_templates/Weekly-Priority-Setting-Kenny.md) from the Retro Thread

/assign @kencjohnston

/label ~"section::ops" ~"Weekly Priorities"

/epic https://gitlab.com/groups/gitlab-com/-/epics/1338
